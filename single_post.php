<?php include('includes/header.php'); ?>

<div class="nubl-section news-nubl">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="f-content-wrapper nubl-content">
                <article class="singlepost-nubl">
                    <h2 class="text-uppercase">Latest Post Blogroll Slider with jQuery and PHP</h2>

                    <div class="singlepost-thumb">
                        <img src="img/generalsaving.jpg" alt="Latest Post Blogroll Slider with jQuery and PHP">
                    </div>
                    <p class="singlepost-desc">
                        NUBL requires its clients to save with most of their loan products and encourages clients to
                        contribute to voluntary savings accounts as well. Both savings accounts give members access to
                        funds for health, education and consumption needs either though withdrawal of voluntary savings.
                        There are 4 types of saving products offered to its clients, they are: compulsory saving,
                        voluntary saving, centre fund savings and recurring savings.
                    </p>
                </article>
            </div>
            <div class="ct-post-nav">
                <div class="ct-post-prev">
                    <strong>Previous:</strong><br> <a href="#" rel="prev">About:  &amp; Nirdhan Utthan Bank</a>
                </div>
                <div class="ct-post-next">
                    <strong>Next:</strong><br> <a href="#" rel="next">About Loan Procedure</a>
                </div>
                <div class="clr"></div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <?php include('includes/sidebar.php'); ?>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
