<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Nirdhan Utthan Bank Limited</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'>
    <link href="css/style.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link rel='stylesheet' href='css/skin-right-thumb.css' type='text/css' />

    <script src="https://use.fontawesome.com/3a86d7ae58.js"></script>
    <script src="js/modernizr.custom.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

    <div class="top-nav">
        <ul>
            <li><a href="resource.php">Resource Center</a></li>
            <li><a href="#">Useful Links</a></li>
            <li><a href="#">Check Mail</a></li>
            <li><a href="branch.php">Locations</a></li>
            <li><a href="about.php">About us</a></li>
        </ul>
    </div>
    <div class="header">
        <div class="row">
            <div class="col-xs-12 col-sm-4 col-md-6">
                <a href="index.php"><img src="img/logo.png" alt="Nirdhan Uthan Bank Limited"></a>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-6">
                <div class="site-menu">
                    <ul>
                        <li><a href="">Career</a></li>
                        <li><a href="contact.php">Contact us</a></li>
                        <li><a href="#">Help</a></li>
                        <li><a href="#">Nepali</a></li>
                        <li>
                            <div class="input-group margin-bottom-sm">
                                <input class="form-control" type="text" placeholder="How can we help you ?">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="main-navigation">
        <nav class="navbar nav-main">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Banking</a>
                        <ul class="dropdown-menu">
                            <li><a href="saving.php">Saving</a></li>
                            <li><a href="#">Microinsurance</a></li>
                        </ul>
                    </li>
                    <li><a href="loan.php">Loan</a></li>
                    <li><a href="#remittance">Remittance</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Rates</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Interest Rates</a></li>
                            <li><a href="#">Currency Rates</a></li>
                        </ul>
                    </li>
                    <li><a href="#contact">Financial Planning</a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                           aria-expanded="false">Media</a>
                        <ul class="dropdown-menu">
                            <li><a href="media.php">Photos</a></li>
                            <li><a href="video.php">Videos</a></li>
                        </ul>
                    </li>
                    <li><a href="#contact">Investment</a></li>
                </ul>
            </div>
        </nav>
    </div>
