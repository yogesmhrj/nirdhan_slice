<?php include('includes/header.php'); ?>

<div class="nubl-section news-nubl">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="f-content-wrapper nubl-content">
                <h2 class="text-uppercase">Nirdhan Latest News and Information</h2>

                <div class="ct-box">
                    <ul>
                        <li>
                            <article class="news-article">

                                <h3><a href="#" class="">Latest Post Blogroll Slider with jQuery and PHP</a></h3>

                                <p class="ct-subline">

                                    By <a href="#">Nirdhan Utthan Bank Limited</a>
                                    <time pubdate="pubdate" class="">June 17, 2010</time>
                                </p>
                                <!--//ct-subline-->

                                <div class="ct-latest-thumb">
                                    <a href="#" class="ct-trans">
                                        <img
                                            src="http://codropspz.tympanus.netdna-cdn.com/codrops/wp-content/uploads/2010/06/latestpostslide-150x150.jpg"
                                            alt="Latest Post Blogroll Slider with jQuery and PHP">
                                    </a>
                                </div>
                                <!--//ct-latest-thumb-->

                                <p class="ct-feat-excerpt">
                                    In this tutorial we are going to create a blogroll slider that shows the latest post
                                    of your favorite blogs. We will be using jQuery, PHP an ...
                                    <span> <a href="#">read more</a></span>
                                </p><!--//ct-feat-excerpt-->

                                <div class="clr"></div>

                            </article>
                        </li>
                        <li>
                            <article class="news-article">

                                <h3><a href="#" class="">Latest Post Blogroll Slider with jQuery and PHP</a></h3>

                                <p class="ct-subline">

                                    By <a href="#">Nirdhan Utthan Bank Limited</a>
                                    <time pubdate="pubdate" class="">June 17, 2010</time>
                                </p>
                                <!--//ct-subline-->

                                <div class="ct-latest-thumb">
                                    <a href="#" class="ct-trans">
                                        <img
                                            src="http://codropspz.tympanus.netdna-cdn.com/codrops/wp-content/uploads/2010/06/latestpostslide-150x150.jpg"
                                            alt="Latest Post Blogroll Slider with jQuery and PHP">
                                    </a>
                                </div>
                                <!--//ct-latest-thumb-->

                                <p class="ct-feat-excerpt">
                                    In this tutorial we are going to create a blogroll slider that shows the latest post
                                    of your favorite blogs. We will be using jQuery, PHP an ...
                                    <span> <a href="#">read more</a></span>
                                </p><!--//ct-feat-excerpt-->

                                <div class="clr"></div>

                            </article>
                        </li>
                        <li>
                            <article class="news-article">

                                <h3><a href="#" class="">Latest Post Blogroll Slider with jQuery and PHP</a></h3>

                                <p class="ct-subline">

                                    By <a href="#">Nirdhan Utthan Bank Limited</a>
                                    <time pubdate="pubdate" class="">June 17, 2010</time>
                                </p>
                                <!--//ct-subline-->

                                <div class="ct-latest-thumb">
                                    <a href="#" class="ct-trans">
                                        <img
                                            src="http://codropspz.tympanus.netdna-cdn.com/codrops/wp-content/uploads/2010/06/latestpostslide-150x150.jpg"
                                            alt="Latest Post Blogroll Slider with jQuery and PHP">
                                    </a>
                                </div>
                                <!--//ct-latest-thumb-->

                                <p class="ct-feat-excerpt">
                                    In this tutorial we are going to create a blogroll slider that shows the latest post
                                    of your favorite blogs. We will be using jQuery, PHP an ...
                                    <span> <a href="#">read more</a></span>
                                </p><!--//ct-feat-excerpt-->

                                <div class="clr"></div>

                            </article>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <?php include('includes/sidebar.php'); ?>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
