<?php include('includes/header.php'); ?>

<div class="nubl-section news-nubl">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="f-content-wrapper nubl-content">
                <h2 class="text-uppercase">Nirdhan Resource Center</h2>
                <div class="resource-section">
                    <article class="resource-article">
                        <a href="#">
                        <h4 class="text-capitalize">NUBL Impact Study 1997</h4>
                            <span class="more">Read More <i class="fa fa-angle-double-right"></i> </span>
                        </a>
                    </article>
                    <article class="resource-article">
                        <a href="#">
                            <h4 class="text-capitalize">NUBL Impact Study 1997</h4>
                            <span class="more">Read More <i class="fa fa-angle-double-right"></i> </span>
                        </a>
                    </article>
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <?php include('includes/sidebar.php'); ?>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
