<?php include('includes/header.php'); ?>

<div class="about-section nubl-section" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="f-common-wrapper">
                <a href="#tab1" class="common-heading select"> <span class="yellow-line"></span>about NUBL <span
                        class="arrow extra-sprite"></span> </a>
                <a href="#tab2" class="common-heading "> <span class="yellow-line"></span>Vision, Mission & Goal<span
                        class="arrow extra-sprite"></span> </a>
                <a href="#tab3" class="common-heading "> <span class="yellow-line"></span>Operation Area <span
                        class="arrow extra-sprite"></span> </a>
                <a href="#tab4" class="common-heading "> <span class="yellow-line"></span>Board of Directors <span
                        class="arrow extra-sprite"></span> </a>
                <a href="#tab5" class="common-heading "> <span class="yellow-line"></span>Management Team
                    <span class="arrow extra-sprite"></span> </a>
                <a href="#tab6" class="common-heading "> <span class="yellow-line"></span>Target Clients
                    <span class="arrow extra-sprite"></span> </a>
                <a href="#tab7" class="common-heading "> <span class="yellow-line"></span>Strategic Alliances
                    <span class="arrow extra-sprite"></span> </a>
                <a href="#tab8" class="common-heading "> <span class="yellow-line"></span>Capital Structure
                    <span class="arrow extra-sprite"></span> </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <div class="f-content-wrapper">
                <div class="content-tab" id="tab1">
                    <h2 class="text-capitalize">About Nirdhan Utthan Bank Limited</h2>

                    <p>
                        Nirdhan Utthan Bank Limited, "the bank for upliftment of the poor" is a microfinance bank
                        established in November 1998 under Company Act of Nepal 1997 (now Company Act 2006). Nepal
                        Rastra Bank, the Central Bank of Nepal, granted a license in April 1999 to undertake banking
                        activities under the Development Bank Act 1996. It started its formal operation from July 1999.
                        Now, operated under Bank and Financial Institutions Act 2006, Nirdhan Utthan Bank Limited (NUBL)
                        provides microfinance services such as Loans, Deposits, Micro-insurance and Remittance services
                        to low income families of Nepal. NUBL follows group lending based on Grameen Bank, Bangladesh
                        model as well as group lending based on NUBL developed Self-Relaint Group model through a
                        network of 178 branch offices spread over all 75 districts of Nepal.
                    </p>

                    <p>

                        Though, legally established as a company in 1998, the operation of NUBL is a continuation of
                        microfinance services provided by an NGO called "NIRDHAN" which was providing microfinance
                        services since March 1993. The story behind the establishment of "NIRDHAN" starts from 1986 when
                        Late Dr. Harihar Dev Pant, then senior officer with the Central Bank of Nepal visited Grameen
                        Bank in Bangladesh. This visit inspired him to launch microfinance program in Nepal resulting
                        the birth of "NIRDHAN" or "People Without Money" in 1991. NIRDHAN began its microfinance
                        operations in March 1993.
                    </p>

                    <p>

                        NIRDHAN, as an NGO had a limited recourses and capacity to satisfy unmet demand of poor people
                        in different part of the country. This resulted the establishment of NUBL as a Company where
                        NIRDHAN NGO became a lead promoter of the bank. In July 1999, NIRDHAN transferred all
                        microfinance operations to NUBL. Strategic reasons for promoting NUBL are:
                    <ul class="about-list">
                        <li>
                            Banks are supervised and regulated by the Central Bank, which will enforce banking
                            standards;
                        </li>
                        <li>
                            A bank can have access to different sources of funding enabling it to satisfy financial need
                            of
                            low income households;
                        </li>
                        <li>
                            The bank can lend to a wider range of clients, including group graduates interested to
                            receive
                            individual loans. Further, a bank can accept collateral for potentially larger and diverse
                            loan
                            products.
                        </li>
                    </ul>
                    </p>
                </div>
                <div class="content-tab" id="tab2">
                    <h2 class="text-capitalize">Vision, Mission & Goal</h2>

                    <h4 class="text-capitalize">Vision</h4>

                    <p>
                        The vision of <strong>Nirdhan Utthan Bank</strong> is to be a bank with a social conscience that
                        enables poor to
                        contribute equally to a prosperous, self-reliant rural society through self-employment and
                        social awareness, and help to reduce poverty in Nepal.
                    </p>
                    <h4 class="text-capitalize">Mission</h4>

                    <p>
                        The mission of <strong>Nirdhan Utthan Bank</strong> is to extend financial services and social
                        awareness to the
                        poor in under-served and un-served areas of Nepal in a sustainable manner.
                    </p>
                    <h4 class="text-capitalize">Goal</h4>

                    <p>
                        The primary goals of <strong>Nirdhan Utthan Bank</strong> are to:
                    <ul class="about-list">
                        <li> Reach a maximum number of poor households with potential and financial viability by
                            adopting proven delivery mechanism.
                        </li>
                        <li> Develop a well-managed institution with high staff morality.
                        </li>
                        <li> Enhance women's "self-respect" through social awareness, proper use & on-time repayments of
                            loans, regular savings and provision of related micro-finance services.
                        </li>
                    </ul>
                    </p>
                </div>
                <div class="content-tab" id="tab3">
                    <h2 class="text-capitalize">Operation Area</h2>

                    <p>
                        NUBL, being a national level <strong>microfinance institution, can operate all over the
                            country</strong>. As of
                        mid- April 2015, it has covered all 75 districts of Nepal through 176 branch offices. Out of
                        3,915 Village Development Committees (VDCs), it has reached 1,436 VDCs spread over plain Trai,
                        Hill & Mountain regions of Nepal.
                    </p>
                </div>
                <div class="content-tab" id="tab4">
                    <h2 class="text-capitalize">Board of Directors</h2>

                    <p>
                        The Board of Directors (BoD) of NUBL comprises seven members, one representing individual
                        promoters, three representing institutional promoters, two representing general public and one
                        independent director. The BoD meets at least 12 times a year and takes policy decisions to
                        pursue the objectives of the bank. The Chairperson of the Board is elected by the board members
                        among themselves. The BoD is responsible to the general meeting of shareholders who meet
                        generally once a year in Annual General Meeting (AGM). The composition of BoD is as follows:
                    </p>
                    <h4 class="text-capitalize">Chairperson</h4>

                    <p>
                        <strong>Mr. Ganesh Bahadur Thapa</strong>- Represents Nirdhan NGO
                    </p>
                    <h4 class="text-capitalize">Directors</h4>

                    <p>
                    <ul class="about-list">
                        <li><strong>Mr. Sushil Joshi</strong> - Represents Himalayan Bank Ltd.</li>
                        <li><strong>Mr. Rajesh Gautam</strong> - Represents Everest Bank Ltd.</li>
                        <li><strong> Mr. Top Bahadur Rayamajhi</strong>- Represents General Public</li>
                        <li><strong>Mr. Achyut Raj Joshi</strong> - Represents General Public</li>
                        <li><strong>Mr. Ram Bhakta Thapa</strong>- Independent Director</li>
                    </ul>
                    </p>
                </div>
                <div class="content-tab" id="tab5">
                    <h2 class="text-capitalize">Management Team</h2>

                    <p>
                        The bank is headed by Chief Executive Officer (CEO) who works as Executive Chief of the bank.
                        The CEO sets vision and strategies and reviews operations carefully. Currently, the Deputy
                        General Manager is responsible for day-to-day operations and works closely with the Chief
                        Executive Officer (CEO). The Chief Executive Officer (CEO) supervises the departmental
                        activities and works in closely with Deputy General Manager. The bank has eight departments at
                        its Central Office, which includes: Human Resources Management and General Administration
                        Department, Operation Department, Internal Audit Department, Planning, Monitoring and Research
                        Department, Accounts and Fund Management Department, Training and Development Department, and
                        Information and Technology Department. The executives and departmental managers are as follows:
                    </p>
                    <h4 class="text-capitalize">Executive Level</h4>

                    <p>
                    <ul class="about-list">
                        <li><strong>Mr. Janardan Dev Pant</strong> - Chief Executive Officer</li>
                        <li><strong>Mr. Bhoj Raj Bashyal</strong> - Deputy General Manager</li>
                        <li><strong>Mr. Raj Narayan Das</strong> - Assistant General Manager</li>
                    </ul>
                    </p>
                    <h4 class="text-capitalize">Managerial Level</h4>

                    <p>
                    <ul class="about-list">
                        <li><strong>Mr. Krishna Raj Chaudhary</strong>, Chief Manager (Company Secretary)
                        </li>
                        <li><strong>Mr. Ram Bahadur Chaudhary</strong>, Chief Manager (Human Resource Department)
                        </li>
                        <li><strong>Mr. Narayan Bahadur Karki</strong>, Senior Manager (Information and Technology
                            Department).
                        </li>
                        <li><strong>Mr. Gokarna Prasad Upadhayay</strong>, Senior Manager (Internal Audit Department)
                        </li>
                        <li><strong>Mr. Mukesh Dahal</strong>, Senior Manager (Accounts and Fund Management Department)
                        </li>
                        <li><strong>Mr. Narayan Prasad Neupane</strong>, Senior Manager (General Admin Department)
                        </li>
                        <li><strong>Mr. Lekhnath Neupane</strong>, Senior Manager (Operation Department)</li>
                        <li><strong>Mr. Pawan Kumar Shrestha</strong>, Senior Manager (Training and Development
                            Department)
                        </li>
                        <li><strong>Mr. Manoj Kumar Yadav</strong>, Act. Senior Manager (Planning, Monitoring and
                            Research Department)
                        </li>
                    </ul>
                    </p>
                </div>
                <div class="content-tab" id="tab6">
                    <h2 class="text-capitalize">Target Clients</h2>

                    <p>
                        Nirdhan Utthan Bank recruits new clients who fit the following targeting criteria for its group
                        based financial services:
                    <ul class="about-list">
                        <li> Own less than 0.25 hectares of irrigated land or less than 0.5 hectares of un-irrigated
                            land per five-person family.
                        </li>
                        <li> House must not have cement walls or concrete ceiling (pucca house).
                        </li>
                        <li> Permanent resident of bank's working area.
                        </li>
                        <li>
                            No family members employed in the formal sector
                        </li>
                    </ul>
                    </p>
                </div>
                <div class="content-tab" id="tab7">
                    <h2 class="text-capitalize">Strategic Alliances</h2>

                    <p>
                        Nirdhan Utthan Bank has established strategic alliances with the following international and
                        national organizations:
                    <ul class="about-list">
                        <li> Grameen Trust, Bangladesh
                        </li>
                        <li> Credit & Saving for Hard-core Poor in Asia & Pacific (CASHPOR), a Philippines based Network
                        </li>
                        <li> Save The Children Federation-USA working closely with Nirdhan/NUBL since July 1997
                        </li>
                        <li>
                            PLAN International began working with Nirdhan/NUBL in October 1998. The partnership
                            continues in capacity building and outreach expansion areas
                            Centre for Micro-Finance (CMF), Nepal
                        </li>
                        <li>
                            The Micro-finance Association of Nepal (MIFAN)
                        </li>
                        <li>
                            The Rural Micro-Finance Development Centre (RMDC), a wholesale lending institution promoted
                            by the Asian Development Bank
                        </li>
                        <li>
                            Nirdhan Utthan Bank contributes to the Micro Banking Bulletin and participant in The MIX
                            (microfinance information exchange)
                        </li>
                        <li>
                            The Consultative Group to Assist the Poor (CGAP) began to work in April 2001 to strengthen
                            internal capacity and systems of Nirdhan Utthan Bank
                        </li>
                        <li>
                            The International Labor Organization (ILO) and NUBL had worked jointly for providing
                            microfinance services to ex-Kamaiyas (bonded labor) in Banke and Dang districts
                        </li>
                        <li>
                            Mercy Corps and Nirdhan Utthan Bank Limited had worked together for "Access to finance in
                            the remote eastern hills of Nepal" and "Global food crisis response project in farwestern
                            part of Nepal"
                        </li>
                        <li>
                            UNDP/UNCDF supported Enhancing Access to Financial Services Project (EAFSP) implemented by
                            Nepal Rastra Bank (Central Bank) to enhance financial services in hilly and remote terrain
                            area
                        </li>
                        <li>
                            USAID supported Nepal Economics Agriculture and Tade Activities (NEAT) project
                        </li>
                        <li>Good Return/World Education Project</li>
                    </ul>
                    </p>
                    <p>
                        Currently, Nirdhan Utthan Bank Ltd. is working together with following national and
                        international organizations:
                    <ul class="about-list">
                        <li>The World Bank Supported AGRIFIN project
                        </li>
                        <li> KIVA
                        </li>
                        <li>International Finance Cooperation (IFC)
                        </li>
                        <li>Whole Planet Foundation (WPF)
                        </li>
                        <li>The Department for International Development (DFID) funded Sustainable Access to Finance And
                            Livelihoods (SAFAL) Project of MERCY-CORPS
                        </li>
                        <li>The Department for International Development (DFID) funded Sustainable Access to Finance And
                            Livelihoods (SAFAL) Project of Blue Berry Hill Charitable Trust (BHCT)/Sambridh Pahad.
                        </li>
                    </ul>
                    </p>
                </div>
                <div class="content-tab" id="tab8">
                    <h2 class="text-capitalize">Equity & Funding</h2>

                    <p>
                        The authorized capital and issued capital of Nirdhan Utthan Bank Limited is Rs. 1.00 billion. As
                        of January, 2016 the paid-up capital is Rs. 500 million. The distribution of shares is as
                        follows:
                    <table class="table table-bordered">
                        <tr>
                            <th>Shareholders</th>
                            <th>Amount</th>
                            <th>% of Total paid-up Equity</th>
                        </tr>
                        <tr>
                            <td>NIRDHAN (The mother NGO)</td>
                            <td> 54,802,352</td>
                            <td> 10.96%</td>
                        </tr>
                        <tr>
                            <td>Himalayan Bank Limited (A Commercial Bank)</td>
                            <td> 69,225,145</td>
                            <td> 11.22%</td>
                        </tr>
                        <tr>
                            <td>Nabil Bank Limited (A Commercial Bank)</td>
                            <td> 56,225,145</td>
                            <td>
                                13.85%
                            </td>
                        </tr>
                        <tr>
                            <td>Everest Bank Limited (A Commercial Bank)</td>
                            <td>56,116,299</td>
                            <td>11.22%</td>
                        </tr>
                        <tr>
                            <td>Grameen Trust Bangladesh</td>
                            <td>17,854,156</td>
                            <td>3.57%</td>
                        </tr>
                        <tr>
                            <td>Private Sector Individuals</td>
                            <td>57,683,050</td>
                            <td>11.54%</td>
                        </tr>
                        <tr>
                            <td>International Finance Corporation (IFC)</td>
                            <td>37,718,013</td>
                            <td>7.54%</td>
                        </tr>
                        <tr>
                            <td>General public including clients and staffs</td>
                            <td>150,484,685</td>
                            <td>30.10%</td>
                        </tr>
                        <tr>
                            <td>Total</td>
                            <td>500,000,000</td>
                            <td>100%</td>
                        </tr>
                    </table>
                    </p>
                    <p>
                        The NIRDHAN, Himalayan Bank Limited, Nabil Bank Limited, Everest Bank Limited, Grameen Trust
                        Bangladesh and private sector individuals are promoters of the bank.
                    </p>

                    <p>
                        The source of fund of NUBL is shareholder's equity, client saving and deposit and borrowing from
                        commercial banks and financial institutions.
                    </p>
                </div>


                <!--                <div class="content-tab" id="tab3">-->
                <!--                    <h2 class="text-capitalize">Message From CEO</h2>-->
                <!---->
                <!--                    <div class="vtable msg-ceo">-->
                <!--                        <div class="table-cell vmiddle">-->
                <!--                            <img src="img/images/udeep-khadka.jpg" alt="Udeep Khadka">-->
                <!--                        </div>-->
                <!--                        <div class="table-cell vmiddle">-->
                <!--                            <h4>Udeep Khadka</h4>-->
                <!--                            <span>CEO, Fourever Enterprises</span><br>-->
                <!--                            <span>+977-9841252518</span>-->
                <!--                            <h5 class="f--moto">Pay only for the Clothing; Satisfaction is for Free�</h5>-->
                <!--                        </div>-->
                <!--                    </div>-->
                <!--                    <p>-->
                <!--                        As we look at the growth over the years since our inception in 2013, we are extremely proud-->
                <!--                        of-->
                <!---->
                <!--                        what we have achieved, and more excited about our outlook for an equally promising future.-->
                <!--                        We-->
                <!---->
                <!--                        are successfully providing our services to become a dignified firm, garnering business from-->
                <!---->
                <!--                        across the nation, while earning our clients&#39; trust .-->
                <!--                    </p>-->
                <!---->
                <!--                    <p>-->
                <!---->
                <!--                        It is satisfying to know that we are able to give our best products to your respected-->
                <!--                        institution,-->
                <!---->
                <!--                        organizations and clients and build good relations with them. Customer satisfaction is the-->
                <!---->
                <!--                        hallmark by which we measure our performance, and we hold ourselves, as do our clients, to-->
                <!--                        the-->
                <!---->
                <!--                        highest standards of quality. In this continually evolving marketplace, our clients are more-->
                <!---->
                <!--                        particular about their choices and more informed than ever about the results supplier-->
                <!--                        supplies.-->
                <!---->
                <!--                        Even so, they continue to select Fourever Enterprises (4Ever) as their partner of choice-->
                <!--                        because-->
                <!---->
                <!--                        of our commitment to quality, good products, and integrity.-->
                <!--                    </p>-->
                <!---->
                <!--                    <p>-->
                <!---->
                <!--                        We value your constructive suggestions and feedbacks very much. And if you have any queries-->
                <!---->
                <!--                        regarding us and our services, please feel free to contact us. We also heartily request and-->
                <!---->
                <!--                        welcome you to pay your visit at our office and see how our team works if you ever wish to.-->
                <!--                        The-->
                <!---->
                <!--                        4Ever Enterprises know and realize your creative part in our business.-->
                <!--                    </p>-->
                <!---->
                <!--                    <p>-->
                <!---->
                <!--                        4Ever thank all who had been there and who are here with us to make this amazing journey-->
                <!---->
                <!--                        possible. Thank you so much for your remarkable companionship. We wish to continue this-->
                <!---->
                <!--                        expedition together with you. Regards,-->
                <!--                    </p>-->
                <!--                </div>-->

            </div>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
