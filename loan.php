<?php include('includes/header.php'); ?>
<div class="vtable img-banner">
    <div class="table-cell vmiddle">
        <h2 class="text-uppercase right">Loan</h2>
    </div>
</div>
<div class="loan-section nubl-section" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-xs-12 col-sm-3 col-md-3">
            <div class="f-common-wrapper">
                <a href="#tab1" class="common-heading select"> <span class="yellow-line"></span>General Loan <span
                        class="arrow extra-sprite"></span> </a>
                <a href="#tab2" class="common-heading "> <span class="yellow-line"></span>Seasonal Agricultural
                    Loan<span
                        class="arrow extra-sprite"></span> </a>
                <a href="#tab3" class="common-heading "> <span class="yellow-line"></span>Seasonal Business Loan <span
                        class="arrow extra-sprite"></span> </a>
                <a href="#tab4" class="common-heading "> <span class="yellow-line"></span>Tube-well/Sanitary Loan <span
                        class="arrow extra-sprite"></span> </a>
                <a href="#tab5" class="common-heading "> <span class="yellow-line"></span>Housing Loan
                    <span class="arrow extra-sprite"></span> </a>
                <a href="#tab6" class="common-heading "> <span class="yellow-line"></span>Micro-Enterprise Loan
                    <span class="arrow extra-sprite"></span> </a>
                <a href="#tab7" class="common-heading "> <span class="yellow-line"></span>Biogas Loan
                    <span class="arrow extra-sprite"></span> </a>
                <a href="#tab8" class="common-heading "> <span class="yellow-line"></span>Foreign-Employment Loan
                    <span class="arrow extra-sprite"></span> </a>
                <a href="#tab9" class="common-heading "> <span class="yellow-line"></span>Education Loan
                    <span class="arrow extra-sprite"></span> </a>
                <a href="#tab10" class="common-heading "> <span class="yellow-line"></span>Emergency Loan
                    <span class="arrow extra-sprite"></span> </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 col-md-9">
            <div class="f-content-wrapper">
                <div class="content-tab" id="tab1">
                    <h2 class="text-capitalize">General Loan</h2>

                    <p>
                        The general loan is a one to two year loan with monthly repayments. Clients make payments mostly
                        during monthly in center meetings composed of up to 10 groups. A center is a meeting place,
                        where group members perform their financial transactions such as repayment, withdrawals, deposit
                        etc. Generally a group is composed of five members. The interest rate is 20% on declining basis
                        if the clients pay the installment in center meeting and 18% if installment paid in branch
                        offices. Before women can become members of NUBL, and hence eligible for loans, they must
                        complete the compulsory group training (lasting from 7-15 days) and pass the group recognition
                        test.
                    </p>

                    <p>

                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered">
                        <tbody>
                        <tr class="th-blue" valign="middle">
                            <th><span style=""><strong>Year</strong></span></th>
                            <th><span style=""><strong>Maximum Loan Size Rs.</strong></span></th>
                            <th><span style=""><strong>Loan Term</strong></span></th>
                            <th><span style=""><strong>Annual Interest Rate on declining balance</strong></span></th>
                            <th><span style=""><strong>Repayment Frequency</strong></span></th>
                        </tr>
                        <tr class="th-normal" valign="middle">
                            <td><span style=""><strong>1</strong></span></td>
                            <td><span style=""><strong>60,000</strong></span></td>
                            <td rowspan="5" style="text-align: center;vertical-align: middle"><span style=""><strong>Maximum
                                        2 years</strong></span></td>
                            <td rowspan="5" style="text-align: center;vertical-align: middle"><span style=""><strong>18
                                        to 20%</strong></span></td>
                            <td rowspan="5" style="text-align: center;vertical-align: middle"><span style=""><strong>Monthly/Quaterly/Ballon</strong></span>
                            </td>
                        </tr>
                        <tr class="th-normal" valign="middle">
                            <td><span style=""><strong>2</strong></span></td>
                            <td><span style=""><strong>100,000</strong></span></td>
                        </tr>
                        <tr class="th-normal" valign="middle">
                            <td><span style=""><strong>3</strong></span></td>
                            <td><span style=""><strong>150,000</strong></span></td>
                        </tr>
                        <tr class="th-normal" valign="middle">
                            <td><span style=""><strong>4</strong></span></td>
                            <td><span style=""><strong>200,000</strong></span></td>
                        </tr>
                        <tr class="th-normal" valign="middle">
                            <td><span style=""><strong>5+</strong></span></td>
                            <td><span style=""><strong>300,000</strong></span></td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab2">
                    <h2 class="text-capitalize">Seasonal Agricultural Loan</h2>


                    <p>
                        This loan is used for purchasing chemical fertilizer, seeds, and other agricultural inputs and
                        is accessible to clients who have repaid 6 months of the general loan with good repayment
                        performance.
                    </p>

                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <th><strong><span>Year</span></strong></th>
                            <th><strong><span>Maximum Loan Size Rs.</span></strong></th>
                            <th><strong><span>Loan Term</span></strong></th>
                            <th><strong><span>Annual Interest Rate on declining balance</span></strong></th>
                            <th><strong><span>Repayment Frequency</span></strong></th>
                        </tr>
                        <tr class="th-normal" valign="top">
                            <td><strong><span>Each year</span></strong></td>
                            <td style="text-align: center;"><strong><span>30000</span></strong></td>
                            <td style="text-align: center;"><strong><span>1 Year</span></strong></td>
                            <td style="text-align: center;"><strong><span>18 to 20%</span></strong></td>
                            <td style="text-align: center;"><strong><span>Monthly/ Quarterly/Balloon</span></strong>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab3">
                    <h2 class="text-capitalize">Seasonal Business Loan</h2>

                    <p>
                        This loan is used for seasonal opportunities like purchasing and selling of agricultural
                        products and animals in festival seasons and is accessible to clients who have successfully
                        completed one cycle of the general loan with good repayment performance.
                    </p>

                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <th><strong><span>Year</span></strong></th>
                            <th><strong><span>Maximum Loan Size Rs.</span></strong></th>
                            <th><strong><span>Loan Term</span></strong></th>
                            <th><strong><span>Annual Interest Rate on declining balance</span></strong></th>
                            <th><strong><span>Repayment Frequency</span></strong></th>
                        </tr>
                        <tr class="th-normal" valign="top">
                            <td><strong><span>Each year</span></strong></td>
                            <td style="text-align: center;"><strong><span>10,000</span></strong></td>
                            <td style="text-align: center;"><strong><span>6 months</span></strong></td>
                            <td style="text-align: center;"><strong><span>18 to 20%</span></strong></td>
                            <td style="text-align: center;"><strong><span>Fortnightly/Monthly/Balloon</span></strong>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab4">
                    <h2 class="text-capitalize">Tube-Well/Sanitary Loan</h2>

                    <p>
                        To improve the health of clients and decrease default related to poor health, NUBL provides a
                        loan to install tube-wells and toilets. It is available to clients who have successfully
                        completed the first loan cycle of the general loan and is only available once for each member.
                    </p>

                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <th><strong><span>Year</span></strong></th>
                            <th><strong><span>Maximum Loan Size Rs.</span></strong></th>
                            <th><strong><span>Loan Term</span></strong></th>
                            <th><strong><span>Annual Interest Rate on declining balance</span></strong></th>
                            <th><strong><span>Repayment Frequency</span></strong></th>
                        </tr>
                        <tr class="th-normal" valign="top">
                            <td><strong><span>Each year</span></strong></td>
                            <td style="text-align: center;"><strong><span>20,000</span></strong></td>
                            <td style="text-align: center;"><strong><span>2 years</span></strong></td>
                            <td style="text-align: center;"><strong><span>18 to 20%</span></strong></td>
                            <td style="text-align: center;"><strong><span>Fortnightly/Monthly</span></strong>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab5">
                    <h2 class="text-capitalize">Housing Loan</h2>

                    <p>
                        NUBL uses a very specific targeting strategy to ensure that all first-time clients are very
                        poor. As one of the targeting criteria is the absence of cement walls or ceiling in the client's
                        house, there is a strong demand for various types of housing loans. To access the housing loans
                        the clients must have maintained strong credit discipline in the previous years, have improved
                        economic condition from previous loans, and be willing to contribute at least 20 percent equity
                        (10 percent cash and 10 percent in-kind). NUBL charges a interest rate of 15 to 18 percent on a
                        declining balance with fortnightly/monthly repayments.
                    <ul class="about-list">

                        <li>House Construction Loan: available to clients who meet the above requirements and have taken
                            general loan. Members generally construct a new house on their current property with this
                            loan.
                        </li>

                        <li>House Repair Loan: available to clients who meet the above requirements and have completed
                            the
                            first loan cycle of the general loan. This loan is used to make capital improvements like
                            replacing a roof on a current house.
                        </li>

                        <li>Homestead Purchase Loan: available to clients who meet the above requirements and have
                            borrowed
                            general loan. This loan allows members to purchase a small amount of land.
                        </li>
                    </ul>


                    The loan size, loan term and other conditions for housing loans are as follows:
                    </p>
                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <th>Loan Type</th>
                            <th>Maximum Loan Size Rs.</th>
                            <th>Loan Term</th>
                            <th>AnnualInterest Rate</th>
                            <th>Repayment Frequency</th>
                        </tr>
                        <tr class="th-normal">
                            <td>House construction</td>
                            <td>1,50,000</td>
                            <td>2-6 Years</td>
                            <td>15% declining</td>
                            <td>Fortnightly/Monthly</td>
                        </tr>
                        <tr class="th-normal">
                            <td>House repair</td>
                            <td>20,000</td>
                            <td>2 Years</td>
                            <td>18% declining</td>
                            <td>Fortnightly/Monthly</td>
                        </tr>
                        <tr class="th-normal">
                            <td>Homestead purchase</td>
                            <td>1,00,000</td>
                            <td>2 Years</td>
                            <td>15% declining</td>
                            <td>Fortnightly/Monthly</td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab6">
                    <h2 class="text-capitalize">Microenterprise Loan</h2>

                    <p>
                        The microenterprise loan was introduced in mid-July 2000. This loan targets graduates of
                        collateral free loans. The clients eligible for this loan must have taken General Loan,
                        maintained strong credit discipline, have improved economic condition from previous loans, be
                        willing to contribute at least 20 percent equity (10 percent cash and 10 percent in-kind),
                        submit collateral, and demonstrate an ability to operate a micro-enterprise. The loan size, term
                        and other conditions for this loan are as follows:

                        The loan size, term and other conditions for this loan are as follows:
                    </p>

                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <th>Maximum Loan Size Rs.</th>
                            <th>Loan Term</th>
                            <th>AnnualInterest Rate</th>
                            <th>Repayment Frequency</th>
                        </tr>
                        <tr class="th-normal">
                            <td>500,000</td>
                            <td>2-3 Years</td>
                            <td>18% declining</td>
                            <td>Fortnightly to Quarterly</td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab7">
                    <h2 class="text-capitalize">Biogas Loan</h2>

                    <p>
                        This loan is used for manufacturing biogas plant. Bank offers this loan to those clients who
                        have livestock and have utilized General loan with good repayment performance. The loan can be
                        given with collateral and without collateral.
                    </p>

                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <td>Maximum Loan Size Rs.</td>
                            <td>Loan Term</td>
                            <td>Annual Interest Rate on declining balance</td>
                            <td>Repayment Frequency</td>
                        </tr>
                        <tr class="th-normal" valign="top">
                            <td>20,000</td>
                            <td>2 - 5 Years</td>
                            <td>15%</td>
                            <td>Fortnightly/Monthly</td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab8">
                    <h2 class="text-capitalize">Foreign Employment Loan</h2>

                    <p>
                        This loan is used for the family member of the client to go abroad for foreign employment. Bank
                        offers this loan to those clients who have utilized General loan with good repayment
                        performance. This is collateral based loan.
                    </p>

                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <td>Maximum Loan Size Rs.</td>
                            <td>Loan Term</td>
                            <td>Annual Interest Rate on declining balance</td>
                            <td>Repayment Frequency</td>
                        </tr>
                        <tr class="th-normal" valign="top">
                            <td>100,000</td>
                            <td>Up to 3 Years</td>
                            <td>15%</td>
                            <td>Fortnightly/Monthly/Quarterly</td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab9">
                    <h2 class="text-capitalize">Education Loan</h2>

                    <p>
                        This loan is used for the children of the client to study and get degree of technical and
                        vocational education. Bank offers this loan to those clients who have utilized General loan with
                        good repayment performance. This is collateral based loan.
                    </p>

                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <td>Maximum Loan Size Rs.</td>
                            <td>Loan Term</td>
                            <td>Annual Interest Rate on declining balance</td>
                            <td>Repayment Frequency</td>
                        </tr>
                        <tr class="th-normal" valign="top">
                            <td>50,000</td>
                            <td>Up to 3 Years</td>
                            <td>15%</td>
                            <td>Fortnightly/Monthly/Quarterly</td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>
                <div class="content-tab" id="tab10">
                    <h2 class="text-capitalize">Emergency Loan</h2>

                    <p>
                        This loan is used to cope the emergency situation happens to the client due to natural
                        calamities. Bank offers this loan to those clients who have affected by natural disasters.
                    </p>

                    <p>
                    <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered" height="40"
                           width="100%">
                        <tbody>
                        <tr class="th-blue" valign="top">
                            <td>Maximum Loan Size Rs.</td>
                            <td>Loan Term</td>
                            <td>Annual Interest Rate on declining balance</td>
                            <td>Repayment Frequency</td>
                        </tr>
                        <tr class="th-normal" valign="top">
                            <td>15,000</td>
                            <td>Up to 2 Years</td>
                            <td>10%</td>
                            <td>Fortnightly/Monthly/Quarterly</td>
                        </tr>
                        </tbody>
                    </table>
                    </p>
                </div>

            </div>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
