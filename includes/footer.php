<div class="footer">
    <nav class="footer-nav">
        <ul class="nav nav-justified">
            <li class="active"><a href="#">Terms & Conditions </a></li>
            <li><a href="#">Privacy</a></li>
            <li><a href="#"> Reports & Publications </a></li>
            <li><a href="#">Branch Network </a></li>
            <li><a href="#">Target Clients </a></li>
            <li><a href="#">News & Updates </a></li>
            <li><a href="#">Contact Us </a></li>
        </ul>
    </nav>
    <div class="foot-bottom">
        <div class="row">
            <div class="col-xs-12 col-sm-7 col-md-7">
                <p>Nirdhan Utthan Bank Limited, "the bank for upliftment of the poor" is a microfinance bank established
                    in
                    November 1998 under Company Act of Nepal 1997 (now Company Act 2006)</p>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-5">
                <div class="social-media right">
                <span>Follow us :
                    <ul>
                        <li><a href="www.facebook.com"><img src="img/icon/fb-icon.png"> </a></li>
                        <li><a href="www.facebook.com"><img src="img/icon/google-icon.png"> </a></li>
                        <li><a href="www.facebook.com"><img src="img/icon/twit-icon.png"> </a></li>
                        <li><a href="www.facebook.com"><img src="img/icon/blog.png"> </a></li>
                        <li><a href="www.facebook.com"><img src="img/icon/youtube-icon.png"> </a></li>
                        <li><a href="www.facebook.com"><img src="img/icon/linkedin-icon.png"> </a></li>
                    </ul>
                </span>
                    <p class="copy">&copy; 2016 Nirdhan utthan Bank</p>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="js/bootstrap.min.js"></script>
<script type='text/javascript' src='js/unitegallery.min.js'></script>
<script src="js/ug-theme-video.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpGridGallery.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->

<script>
    $(document).ready(function () {
        $('ul.nav li.dropdown').hover(function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function () {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });
    });
</script>
<script>
    $(document).ready(function () {
        $(".f-common-wrapper a").click(function (event) {
            event.preventDefault();
            $(this).addClass("select");
            $(this).siblings().removeClass("select");
            var tab = $(this).attr("href");
            $(".content-tab").not(tab).css("display", "none");
            $(tab).fadeIn();
        });
    });
</script>

<script>
    new CBPGridGallery(document.getElementById('grid-gallery'));
</script>
<script type="text/javascript">

    jQuery(document).ready(function () {

        jQuery("#gallery").unitegallery();

    });

</script>
</body>
</html>
