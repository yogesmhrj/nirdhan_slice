<?php $thisPage = "Contact"; ?>
<?php include('includes/header.php'); ?>
<div class="vtable img-banner map-banner">
    <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3552.5691181751886!2d84.91734601423096!3d27.075345559589547!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3993555df779f7f9%3A0xb26f99f6a89940b!2sNirdhan+Utthan+Bank+Limited!5e0!3m2!1sen!2sdk!4v1469780865765"
        width="100%" height="232px" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<div class="contact-section nubl-section">
    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7">
            <div class="formWrap">

                <form method="post" class="form form-horizontal" action="#" id="contactForm" name="contactForm"
                      autocomplete="off" novalidate="novalidate">
                    <div class="form-group">
                        <label for="Last Name" class="lab col-sm-3">Full Name:</label>

                        <div class="col-sm-9">
                            <input type="text" name="full_name" id="full_name" class="form-control required"
                                   minlength="3"
                                   placeholder="Enter your full name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Email" class="lab col-sm-3">Email:</label>

                        <div class="col-sm-9">
                            <input type="text" name="email" id="email" class="form-control required"
                                   placeholder="Enter your email" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Phone" class="lab col-sm-3">Phone:</label>

                        <div class="col-sm-9">
                            <input type="text" name="phone" id="phone" class="form-control"
                                   placeholder="Enter your contact number" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="postal-address" class="lab col-sm-3">Postal Address:</label>

                        <div class="col-sm-9">
                            <textarea name="postal_address" id="postal_address"
                                      placeholder="Enter your Postal Address here"
                                      class="form-control required" minlength="15"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Message" class="lab col-sm-3">Message:</label>

                        <div class="col-sm-9">
                            <textarea name="message" id="message" placeholder="Enter your message here"
                                      class=" form-control required" minlength="15" style="height:200px;"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Security Code" class="lab col-sm-3">Security Code:</label>

                        <div class="col-sm-3">
                            <img style="height:40px"
                                 src="http://www.nirdhan.com/includes/captcha/CaptchaSecurityImages.php?width=120&amp;height=40&amp;characters=5">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="Enter Above Code" class="lab col-sm-3">Enter Above Code:</label>

                        <div class="col-sm-9">
                            <input name="security_code" type="text" id="security_code"
                                   class="form-control security required"
                                   minlength="5"
                                   maxlength="5">
                        </div>
                    </div>
                    <label for="sumit" class="lab">&nbsp;</label>
                    <input type="submit" name="submit" value="Submit" class="col-sm-3 btn btn-primary">
                    <input type="hidden" name="method" value="contact_us">
                    <input type="hidden" name="id" value="contact-us">

                    <div class="clear"></div>
                </form>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5">
            <div class="f-content-wrapper">
                <h2 class="text-capitalize">Contact us</h2>
                <h4 class="text-capitalize">Nirdhan Utthan Bank Limited</h4>
                <strong>Nirdhan Bhawan</strong><br>
                <span>Bhagwatibahal, Naxal -1</span><br>
                <span>Kathmandu, Nepal</span><br>
                <span><strong>Phone No. :</strong> 977-1- 4413711, 4413794, 4413840</span><br>
                <span><strong>Fax No. :</strong> 977- 1- 4413856</span><br>
                <span><strong>Email:</strong> <a href="mailto:info@nirdhan.com">info@nirdhan.com</a> </span>
            </div>
        </div>
    </div>
</div>

<?php include('includes/footer.php'); ?>
