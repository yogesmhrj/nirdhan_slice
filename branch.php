<?php include('includes/header.php'); ?>

<div class="nubl-section news-nubl">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="f-content-wrapper nubl-content">
                <h2 class="text-uppercase">Nirdhan Branch Network</h2>

                <div class="content-detail">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       aria-expanded="true" aria-controls="collapseOne">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Regional Office Bhairahwa, Rupandehi</th>
                                                <th>071-527664</th>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped" width="100%">
                                        <tbody>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Regional Offices/Branches</th>
                                            <th>Phone</th>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td>A</td>
                                            <td>Regional Office Bhairahwa, Rupandehi</td>
                                            <td>071-527664</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Aryabhanjyang, Palpa</td>
                                            <td>075-402064</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Banstari, Palpa</td>
                                            <td>071-696976</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Belatari, Nawalparasi</td>
                                            <td>078-620614</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Bhairahwa, Rupandehi</td>
                                            <td>071-527439</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Butwal, Rupandehi</td>
                                            <td>071-546852</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Chahara, palpa</td>
                                            <td>071-694351</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Dhakdhai, Rupandehi</td>
                                            <td>071-691755</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Farsatikar, Rupandehi</td>
                                            <td>071-696978</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Haraiya, Rupandehi</td>
                                            <td>071-417018</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Imiliya, Kapilvastu</td>
                                            <td>071-691558</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Kotihawa, Rupandehi</td>
                                            <td>071-561928</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Mahajidiya, Rupandehi</td>
                                            <td>071-691558</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Majhgawa, Rupandehi</td>
                                            <td>071-691282</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>Odari, Kapilvastu</td>
                                            <td>076-691020</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Parasi, Nawalparasi</td>
                                            <td>078-520297</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Ramapur, Rupandehi</td>
                                            <td>071-440167</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Sandhikharka, Arghakhanchi</td>
                                            <td>077-420793</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Sitalnagar, Rupandehi</td>
                                            <td>071-577095</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Regional Office Bharatpur, Chitwan</th>
                                                <th>056-525256</th>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped" width="100%">
                                        <tbody>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Regional Offices/Branches</th>
                                            <th>Phone</th>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td>B</td>
                                            <td>Regional Office Bharatpur, Chitwan</td>
                                            <td>056-525256</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Bhandara, Chitwan</td>
                                            <td>056-550111</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Bharatpur, Chitwan</td>
                                            <td>056-527709</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Chanauli, Chitwan</td>
                                            <td>056-592427</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Daldale, Nawalparasi</td>
                                            <td>078-575066</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Madi, Chitwan</td>
                                            <td>056-692171</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Mungling, Chitwan</td>
                                            <td>056-540171</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Tandi, Chitwan</td>
                                            <td>056-561674</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">

                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Regional Office Birgunj, Parsa</th>
                                                <th>051-529651</th>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped" width="100%">
                                        <tbody>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Regional Offices/Branches</th>
                                            <th>Phone</th>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Basantpatti, Rautahat</td>
                                            <td>055-690478</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Bindabassini, Parsa</td>
                                            <td>051-621181</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Birgunj, Parsa</td>
                                            <td>051-531097</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Biruwaguthi, parsa</td>
                                            <td>053-692452</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Chandranigahpur, Rautahat</td>
                                            <td>055-540341</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Ganjbhawanipur, Bara</td>
                                            <td>053-690125</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Garuda, Rautahat</td>
                                            <td>055-565119</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Gaur, Rautahat</td>
                                            <td>055-521179</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Hetauda, Makwanpur</td>
                                            <td>057-526541</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Kalaiya, Bara</td>
                                            <td>053-551012</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Kolbhi, Bara</td>
                                            <td>053-691604</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Parsauni, Bara</td>
                                            <td>9755001256</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Parwanipur, Bara</td>
                                            <td>9755001103</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Pokhariya, Parsa</td>
                                            <td>9755001208</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Simara, Bara</td>
                                            <td>053-521823</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>Simraungadh, Bara</td>
                                            <td>053-620453</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Sripur, Parsa</td>
                                            <td>051-691480</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Telkuwa, Bara</td>
                                            <td>053-620143</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse4" aria-expanded="false" aria-controls="collapseTwo">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Regional Office Kohalpur, Banke</th>
                                                <th>081-541799</th>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped" width="100%">
                                        <tbody>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Regional Offices/Branches</th>
                                            <th>Phone</th>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Bansgadhi, Bardiya</td>
                                            <td>084-540051</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Bhingribazzar, Pyuthan</td>
                                            <td>086-400014</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Bhurigaun, Bardiya</td>
                                            <td>084-403031</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Bijuwar, Pyuthan</td>
                                            <td>084-403031</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Fatepur, Banke</td>
                                            <td>081-621256</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Gadhawa, Dang</td>
                                            <td>082-691198</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Ghorahi, Dang</td>
                                            <td>082-563333</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Gulariya, Bardiya</td>
                                            <td>084-421147</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Khajura, Banke</td>
                                            <td>081-560373</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Kohalpur, Banke</td>
                                            <td>081-540652</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Paraspur, Banke</td>
                                            <td>081-521562</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Rajapur, Bardiya</td>
                                            <td>084-460102</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Ranjha, Banke</td>
                                            <td>081-565317</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Sisahaniya, Dang</td>
                                            <td>082-402042</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Sulichaur, Rolpa</td>
                                            <td>086-401067</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>Tulsipur, Dang</td>
                                            <td>082-522240</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Udaypur, Banke</td>
                                            <td>081-526941</td>
                                        </tr>
                                        <tr>
                                            <td>E</td>
                                            <td>Regional Office Birtamode, Jhapa</td>
                                            <td>023-543252</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Bhojpur</td>
                                            <td>029-420736</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Biratchowk, Morang</td>
                                            <td>021-545783</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Biratnagar, Morang</td>
                                            <td>021-463041</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Chainpur, Sankhuwasabha</td>
                                            <td>029-570370</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Chandragadhi, Jhapa</td>
                                            <td>023-456895</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Damak, Jhapa</td>
                                            <td>023-580824</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Dhulabari, Jhapa</td>
                                            <td>023-560556</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Phidim, Panchthar</td>
                                            <td>024-520503</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Fikkal, Ilam</td>
                                            <td>027-540457</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Fungling, Taplejung</td>
                                            <td>024-460629</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Gauradaha, Jhapa</td>
                                            <td>023-480324</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Hile, Dhankutta</td>
                                            <td>026-540493</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Ilam, Ilam</td>
                                            <td>027-521718</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Jhumka, Sunsari</td>
                                            <td>025-562339</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Khandbari, sankhuwasabha</td>
                                            <td>029-560954</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>Myanglung, Terahthum</td>
                                            <td>026-460724</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Ravi, Panchthar</td>
                                            <td>024- 691131</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Surunga, Jhapa</td>
                                            <td>023-550837</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Tankisinuwari, Morang</td>
                                            <td>021-421207</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Yashok, Panchthar</td>
                                            <td>024- 691134</td>
                                        </tr>
                                        <tr>
                                            <td>F</td>
                                            <td>Regional Office Bardibas, Mahottari</td>
                                            <td>044-550484</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Aurahi, Mahottari</td>
                                            <td>044-692154</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Barhathawa, Sarlahi</td>
                                            <td>046-540040</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Beltar, Udaypur</td>
                                            <td>035-440346</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Dhanushadham, Dhanusha</td>
                                            <td>041-693800</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Diktel, Khotang</td>
                                            <td>036-420688</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Gaushala, Mahottari</td>
                                            <td>044- 560085</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Godaita, Sarlahi</td>
                                            <td>046- 691995</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Jaleshwar, Mahottari</td>
                                            <td>044-520479</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Janakpur, Dhanusha</td>
                                            <td>041-528578</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Kanchanpur, Saptari</td>
                                            <td>031-560054</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Kathauna, Saptari</td>
                                            <td>031-410018</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Khurkot, Sindhuli</td>
                                            <td>047-691567</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Lahan, Siraha</td>
                                            <td>033-561573</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Mahendranagar, Dhanusha</td>
                                            <td>041-540510</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Murkuchi, Udaypur</td>
                                            <td>035-691257</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>Nawalpur, Sarlahi</td>
                                            <td>046-620435</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Okhaldhunga,Okhaldhunga</td>
                                            <td>9844964118</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Saghutaar, Ramechhap</td>
                                            <td>9847828770</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Sukhipur, Siraha</td>
                                            <td>033-693825</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Yedukaha, Dhanusha</td>
                                            <td>9816276971</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Salleri Branch, Solukhumbu</td>
                                            <td>9842727773</td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>Halesi Branch, Khotang</td>
                                            <td>9847050958</td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>Bodebasain Branch, Saptari</td>
                                            <td>9815444988</td>
                                        </tr>
                                        <tr>
                                            <td>24</td>
                                            <td>Bandipur Branch, Siraha</td>
                                            <td>9845183827</td>
                                        </tr>
                                        <tr>
                                            <td>25</td>
                                            <td>Chaulikha Branch, Sarlahi</td>
                                            <td>9817419787</td>
                                        </tr>
                                        <tr>
                                            <td>26</td>
                                            <td>Choharwa Branch, Siraha</td>
                                            <td>9845120331</td>
                                        </tr>
                                        <tr>
                                            <td>G</td>
                                            <td>Regional Office Attariya, Kailai</td>
                                            <td>091-551260</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Amargadhi, Dadeldhura</td>
                                            <td>096-420269</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Attariya, kailali</td>
                                            <td>091-550471</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Belauri, Kanchanpur</td>
                                            <td>099-580043</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Darshatchand, Baitadi</td>
                                            <td>095-520323</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Fulbari, Kailali</td>
                                            <td>9847052593</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Jhalari, Kanchanpur</td>
                                            <td>099-540201</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Jogbudha, Dadeldhura</td>
                                            <td>9759502272</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Joshipur, Kailali</td>
                                            <td>091-401081</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Lamki, Kailali</td>
                                            <td>091-540321</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Mahendranagar, Kanchanpur</td>
                                            <td>099-520570</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Mangalsen, Accham</td>
                                            <td>9868015152</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Martadi, Bajura</td>
                                            <td>9748052880</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Masuriya, Kailali</td>
                                            <td>091-402114</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Silgadhi, Doti</td>
                                            <td>094-420520</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Sukhad, Kailali</td>
                                            <td>091-403042</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>SwasthyaChauki, Kanchanpur</td>
                                            <td>099-400026</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Tikapur, Kailali</td>
                                            <td>091- 560105</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Gokuleswor Branch, Darchula</td>
                                            <td>9847424430</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Chainpur Branch, Bajhang</td>
                                            <td>9848766932</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Sanphebagar Branch, Achham</td>
                                            <td>9847882310</td>
                                        </tr>
                                        <tr>
                                            <td>H</td>
                                            <td>Regional Office Birendranagar, Surkhet</td>
                                            <td>083-523746</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Birendranagar, Surkhet</td>
                                            <td>083-523342</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Chaurjahari, Rukum</td>
                                            <td>088-401052</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Jahre, Surkhet</td>
                                            <td>9758002930</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Narayan, Dailekh</td>
                                            <td>089-420457</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Dolpa, Dolpa</td>
                                            <td>087 550118</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Jumla, Jumla</td>
                                            <td>087-520488</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Khalanga, Jajarkot</td>
                                            <td>9857024212</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Manma, Kalikot</td>
                                            <td>9848733358</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Salibazar, Salyan</td>
                                            <td>9809586716</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Simikot Branch, Humla</td>
                                            <td>9748022907</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Gamgadhi Branch, Mugu</td>
                                            <td>9748002000</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Dullu Branch, Dailekh</td>
                                            <td>089-411051</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Bidhyapur Branch, Surkhet</td>
                                            <td>9848027151</td>
                                        </tr>
                                        <tr>
                                            <td>I</td>
                                            <td>Regional Office Pokhara, Kaski</td>
                                            <td>061-541380</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Babiyachaur, Myagdi</td>
                                            <td>068-690755</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Burtibang, Baglung</td>
                                            <td>068-410023</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Dulegauda, Tanahu</td>
                                            <td>065-570860</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Galkot, Baglung</td>
                                            <td>068-690754</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Galyang, Syangja</td>
                                            <td>063-460287</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Patichaur, Parbat</td>
                                            <td>067-690583</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Pokhara, Kaski</td>
                                            <td>061-533243</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Putlibazzar, Syangja</td>
                                            <td>063-420922</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Rahale, Parbat</td>
                                            <td>9815253193</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Waling, Syangja</td>
                                            <td>063-440514</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Chame Branch, Manang</td>
                                            <td>9846197452</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Ghansa Branch, Mustang</td>
                                            <td>9847317692</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Gaaikhur Branch, Gorkha</td>
                                            <td>9845221568</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Duipiple Branch, Lamjung</td>
                                            <td>9845079788</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Kharwang Branch, Baglung</td>
                                            <td>9846065955</td>
                                        </tr>
                                        <tr>
                                            <td>J</td>
                                            <td>Regional Office Kathmandu, Kathmandu</td>
                                            <td>01-4415257</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Galchhi, Dhadhing</td>
                                            <td>010-403017</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Dhikure, Nuwakot</td>
                                            <td>010-681339</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Chhatrebajh, Kavreplanchowk</td>
                                            <td>011-685823</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Chhaling, Bhaktapur</td>
                                            <td>01-5091020</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Dakshinkali, Kathmandu</td>
                                            <td>01-4710505</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Gagalphedi, Kathmandu</td>
                                            <td>9845253623</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Jethal, Sindhupalchowk</td>
                                            <td>9842506965</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Namdu, Dolakha</td>
                                            <td>9847079664</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Nawalpur, Sindhupalchowk</td>
                                            <td>9851176430</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Ramkot, Kathmandu</td>
                                            <td>9847027217</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Tikathali, Lalitpur</td>
                                            <td>9847029530</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Tripureshor, Dhading</td>
                                            <td>9847040850</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Thecho, Lalitpur</td>
                                            <td>9818146291</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Kalikasthan Branch, Rasuwa</td>
                                            <td>9847848298</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Palung Branch, Makwanpur</td>
                                            <td>9851054626</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Daunnedevi Branch, Nawalparasi</td>
                                            <td>078-620316</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Jabgadi,Palpa</td>
                                            <td>071-696976</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Bijaybasti Branch, Parsa</td>
                                            <td>053-691604</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Juhaang,Gulmi</td>
                                            <td>071-696978</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Paanchkanya Branch, Sunsari</td>
                                            <td>025-552131</td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>Karsiya Branch, Morang</td>
                                            <td>021-565248</td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>Prakashpur Branch, Sunsari</td>
                                            <td>021-565248</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Regional Office Birtamode, Jhapa</th>
                                                <th>023-543252</th>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse5" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped" width="100%">
                                        <tbody>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Regional Offices/Branches</th>
                                            <th>Phone</th>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Bhojpur</td>
                                            <td>029-420736</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Biratchowk, Morang</td>
                                            <td>021-545783</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Biratnagar, Morang</td>
                                            <td>021-463041</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Chainpur, Sankhuwasabha</td>
                                            <td>029-570370</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Chandragadhi, Jhapa</td>
                                            <td>023-456895</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Damak, Jhapa</td>
                                            <td>023-580824</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Dhulabari, Jhapa</td>
                                            <td>023-560556</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Phidim, Panchthar</td>
                                            <td>024-520503</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Fikkal, Ilam</td>
                                            <td>027-540457</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Fungling, Taplejung</td>
                                            <td>024-460629</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Gauradaha, Jhapa</td>
                                            <td>023-480324</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Hile, Dhankutta</td>
                                            <td>026-540493</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Ilam, Ilam</td>
                                            <td>027-521718</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Jhumka, Sunsari</td>
                                            <td>025-562339</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Khandbari, sankhuwasabha</td>
                                            <td>029-560954</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>Myanglung, Terahthum</td>
                                            <td>026-460724</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Ravi, Panchthar</td>
                                            <td>024- 691131</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Surunga, Jhapa</td>
                                            <td>023-550837</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Tankisinuwari, Morang</td>
                                            <td>021-421207</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Yashok, Panchthar</td>
                                            <td>024- 691134</td>
                                        </tr>
                                        <tr>
                                            <td>F</td>
                                            <td>Regional Office Bardibas, Mahottari</td>
                                            <td>044-550484</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Aurahi, Mahottari</td>
                                            <td>044-692154</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Barhathawa, Sarlahi</td>
                                            <td>046-540040</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Beltar, Udaypur</td>
                                            <td>035-440346</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Dhanushadham, Dhanusha</td>
                                            <td>041-693800</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Diktel, Khotang</td>
                                            <td>036-420688</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Gaushala, Mahottari</td>
                                            <td>044- 560085</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Godaita, Sarlahi</td>
                                            <td>046- 691995</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Jaleshwar, Mahottari</td>
                                            <td>044-520479</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Janakpur, Dhanusha</td>
                                            <td>041-528578</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Kanchanpur, Saptari</td>
                                            <td>031-560054</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Kathauna, Saptari</td>
                                            <td>031-410018</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Khurkot, Sindhuli</td>
                                            <td>047-691567</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Lahan, Siraha</td>
                                            <td>033-561573</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Mahendranagar, Dhanusha</td>
                                            <td>041-540510</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Murkuchi, Udaypur</td>
                                            <td>035-691257</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>Nawalpur, Sarlahi</td>
                                            <td>046-620435</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Okhaldhunga,Okhaldhunga</td>
                                            <td>9844964118</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Saghutaar, Ramechhap</td>
                                            <td>9847828770</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Sukhipur, Siraha</td>
                                            <td>033-693825</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Yedukaha, Dhanusha</td>
                                            <td>9816276971</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Salleri Branch, Solukhumbu</td>
                                            <td>9842727773</td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>Halesi Branch, Khotang</td>
                                            <td>9847050958</td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>Bodebasain Branch, Saptari</td>
                                            <td>9815444988</td>
                                        </tr>
                                        <tr>
                                            <td>24</td>
                                            <td>Bandipur Branch, Siraha</td>
                                            <td>9845183827</td>
                                        </tr>
                                        <tr>
                                            <td>25</td>
                                            <td>Chaulikha Branch, Sarlahi</td>
                                            <td>9817419787</td>
                                        </tr>
                                        <tr>
                                            <td>26</td>
                                            <td>Choharwa Branch, Siraha</td>
                                            <td>9845120331</td>
                                        </tr>
                                        <tr>
                                            <td>G</td>
                                            <td>Regional Office Attariya, Kailai</td>
                                            <td>091-551260</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Amargadhi, Dadeldhura</td>
                                            <td>096-420269</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Attariya, kailali</td>
                                            <td>091-550471</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Belauri, Kanchanpur</td>
                                            <td>099-580043</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Darshatchand, Baitadi</td>
                                            <td>095-520323</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Fulbari, Kailali</td>
                                            <td>9847052593</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Jhalari, Kanchanpur</td>
                                            <td>099-540201</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Jogbudha, Dadeldhura</td>
                                            <td>9759502272</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Joshipur, Kailali</td>
                                            <td>091-401081</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Lamki, Kailali</td>
                                            <td>091-540321</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Mahendranagar, Kanchanpur</td>
                                            <td>099-520570</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Mangalsen, Accham</td>
                                            <td>9868015152</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Martadi, Bajura</td>
                                            <td>9748052880</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Masuriya, Kailali</td>
                                            <td>091-402114</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Silgadhi, Doti</td>
                                            <td>094-420520</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Sukhad, Kailali</td>
                                            <td>091-403042</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>SwasthyaChauki, Kanchanpur</td>
                                            <td>099-400026</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Tikapur, Kailali</td>
                                            <td>091- 560105</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Gokuleswor Branch, Darchula</td>
                                            <td>9847424430</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Chainpur Branch, Bajhang</td>
                                            <td>9848766932</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Sanphebagar Branch, Achham</td>
                                            <td>9847882310</td>
                                        </tr>
                                        <tr>
                                            <td>H</td>
                                            <td>Regional Office Birendranagar, Surkhet</td>
                                            <td>083-523746</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Birendranagar, Surkhet</td>
                                            <td>083-523342</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Chaurjahari, Rukum</td>
                                            <td>088-401052</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Jahre, Surkhet</td>
                                            <td>9758002930</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Narayan, Dailekh</td>
                                            <td>089-420457</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Dolpa, Dolpa</td>
                                            <td>087 550118</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Jumla, Jumla</td>
                                            <td>087-520488</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Khalanga, Jajarkot</td>
                                            <td>9857024212</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Manma, Kalikot</td>
                                            <td>9848733358</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Salibazar, Salyan</td>
                                            <td>9809586716</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Simikot Branch, Humla</td>
                                            <td>9748022907</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Gamgadhi Branch, Mugu</td>
                                            <td>9748002000</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Dullu Branch, Dailekh</td>
                                            <td>089-411051</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Bidhyapur Branch, Surkhet</td>
                                            <td>9848027151</td>
                                        </tr>
                                        <tr>
                                            <td>I</td>
                                            <td>Regional Office Pokhara, Kaski</td>
                                            <td>061-541380</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Babiyachaur, Myagdi</td>
                                            <td>068-690755</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Burtibang, Baglung</td>
                                            <td>068-410023</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Dulegauda, Tanahu</td>
                                            <td>065-570860</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Galkot, Baglung</td>
                                            <td>068-690754</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Galyang, Syangja</td>
                                            <td>063-460287</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Patichaur, Parbat</td>
                                            <td>067-690583</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Pokhara, Kaski</td>
                                            <td>061-533243</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Putlibazzar, Syangja</td>
                                            <td>063-420922</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Rahale, Parbat</td>
                                            <td>9815253193</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Waling, Syangja</td>
                                            <td>063-440514</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Chame Branch, Manang</td>
                                            <td>9846197452</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Ghansa Branch, Mustang</td>
                                            <td>9847317692</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Gaaikhur Branch, Gorkha</td>
                                            <td>9845221568</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Duipiple Branch, Lamjung</td>
                                            <td>9845079788</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Kharwang Branch, Baglung</td>
                                            <td>9846065955</td>
                                        </tr>
                                        <tr>
                                            <td>J</td>
                                            <td>Regional Office Kathmandu, Kathmandu</td>
                                            <td>01-4415257</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Galchhi, Dhadhing</td>
                                            <td>010-403017</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Dhikure, Nuwakot</td>
                                            <td>010-681339</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Chhatrebajh, Kavreplanchowk</td>
                                            <td>011-685823</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Chhaling, Bhaktapur</td>
                                            <td>01-5091020</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Dakshinkali, Kathmandu</td>
                                            <td>01-4710505</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Gagalphedi, Kathmandu</td>
                                            <td>9845253623</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Jethal, Sindhupalchowk</td>
                                            <td>9842506965</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Namdu, Dolakha</td>
                                            <td>9847079664</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Nawalpur, Sindhupalchowk</td>
                                            <td>9851176430</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Ramkot, Kathmandu</td>
                                            <td>9847027217</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Tikathali, Lalitpur</td>
                                            <td>9847029530</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Tripureshor, Dhading</td>
                                            <td>9847040850</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Thecho, Lalitpur</td>
                                            <td>9818146291</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Kalikasthan Branch, Rasuwa</td>
                                            <td>9847848298</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Palung Branch, Makwanpur</td>
                                            <td>9851054626</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Daunnedevi Branch, Nawalparasi</td>
                                            <td>078-620316</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Jabgadi,Palpa</td>
                                            <td>071-696976</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Bijaybasti Branch, Parsa</td>
                                            <td>053-691604</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Juhaang,Gulmi</td>
                                            <td>071-696978</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Paanchkanya Branch, Sunsari</td>
                                            <td>025-552131</td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>Karsiya Branch, Morang</td>
                                            <td>021-565248</td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>Prakashpur Branch, Sunsari</td>
                                            <td>021-565248</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                        <table class="table table-bordered">
                                            <tr>
                                                <th>Regional Office Bardibas, Mahottari</th>
                                                <th>044-550484</th>
                                            </tr>
                                        </table>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse6" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped" width="100%">
                                        <tbody>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Regional Offices/Branches</th>
                                            <th>Phone</th>
                                        </tr>
                                        </tbody>
                                        <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>Aurahi, Mahottari</td>
                                            <td>044-692154</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Barhathawa, Sarlahi</td>
                                            <td>046-540040</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Beltar, Udaypur</td>
                                            <td>035-440346</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Dhanushadham, Dhanusha</td>
                                            <td>041-693800</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Diktel, Khotang</td>
                                            <td>036-420688</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Gaushala, Mahottari</td>
                                            <td>044- 560085</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Godaita, Sarlahi</td>
                                            <td>046- 691995</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Jaleshwar, Mahottari</td>
                                            <td>044-520479</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Janakpur, Dhanusha</td>
                                            <td>041-528578</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Kanchanpur, Saptari</td>
                                            <td>031-560054</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Kathauna, Saptari</td>
                                            <td>031-410018</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Khurkot, Sindhuli</td>
                                            <td>047-691567</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Lahan, Siraha</td>
                                            <td>033-561573</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Mahendranagar, Dhanusha</td>
                                            <td>041-540510</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Murkuchi, Udaypur</td>
                                            <td>035-691257</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>Nawalpur, Sarlahi</td>
                                            <td>046-620435</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Okhaldhunga,Okhaldhunga</td>
                                            <td>9844964118</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Saghutaar, Ramechhap</td>
                                            <td>9847828770</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Sukhipur, Siraha</td>
                                            <td>033-693825</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Yedukaha, Dhanusha</td>
                                            <td>9816276971</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Salleri Branch, Solukhumbu</td>
                                            <td>9842727773</td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>Halesi Branch, Khotang</td>
                                            <td>9847050958</td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>Bodebasain Branch, Saptari</td>
                                            <td>9815444988</td>
                                        </tr>
                                        <tr>
                                            <td>24</td>
                                            <td>Bandipur Branch, Siraha</td>
                                            <td>9845183827</td>
                                        </tr>
                                        <tr>
                                            <td>25</td>
                                            <td>Chaulikha Branch, Sarlahi</td>
                                            <td>9817419787</td>
                                        </tr>
                                        <tr>
                                            <td>26</td>
                                            <td>Choharwa Branch, Siraha</td>
                                            <td>9845120331</td>
                                        </tr>
                                        <tr>
                                            <td>G</td>
                                            <td>Regional Office Attariya, Kailai</td>
                                            <td>091-551260</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Amargadhi, Dadeldhura</td>
                                            <td>096-420269</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Attariya, kailali</td>
                                            <td>091-550471</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Belauri, Kanchanpur</td>
                                            <td>099-580043</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Darshatchand, Baitadi</td>
                                            <td>095-520323</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Fulbari, Kailali</td>
                                            <td>9847052593</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Jhalari, Kanchanpur</td>
                                            <td>099-540201</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Jogbudha, Dadeldhura</td>
                                            <td>9759502272</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Joshipur, Kailali</td>
                                            <td>091-401081</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Lamki, Kailali</td>
                                            <td>091-540321</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Mahendranagar, Kanchanpur</td>
                                            <td>099-520570</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Mangalsen, Accham</td>
                                            <td>9868015152</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Martadi, Bajura</td>
                                            <td>9748052880</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Masuriya, Kailali</td>
                                            <td>091-402114</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Silgadhi, Doti</td>
                                            <td>094-420520</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Sukhad, Kailali</td>
                                            <td>091-403042</td>
                                        </tr>
                                        <tr>
                                            <td>16</td>
                                            <td>SwasthyaChauki, Kanchanpur</td>
                                            <td>099-400026</td>
                                        </tr>
                                        <tr>
                                            <td>17</td>
                                            <td>Tikapur, Kailali</td>
                                            <td>091- 560105</td>
                                        </tr>
                                        <tr>
                                            <td>18</td>
                                            <td>Gokuleswor Branch, Darchula</td>
                                            <td>9847424430</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Chainpur Branch, Bajhang</td>
                                            <td>9848766932</td>
                                        </tr>
                                        <tr>
                                            <td>20</td>
                                            <td>Sanphebagar Branch, Achham</td>
                                            <td>9847882310</td>
                                        </tr>
                                        <tr>
                                            <td>H</td>
                                            <td>Regional Office Birendranagar, Surkhet</td>
                                            <td>083-523746</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Birendranagar, Surkhet</td>
                                            <td>083-523342</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Chaurjahari, Rukum</td>
                                            <td>088-401052</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Jahre, Surkhet</td>
                                            <td>9758002930</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Narayan, Dailekh</td>
                                            <td>089-420457</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Dolpa, Dolpa</td>
                                            <td>087 550118</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Jumla, Jumla</td>
                                            <td>087-520488</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Khalanga, Jajarkot</td>
                                            <td>9857024212</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Manma, Kalikot</td>
                                            <td>9848733358</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Salibazar, Salyan</td>
                                            <td>9809586716</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Simikot Branch, Humla</td>
                                            <td>9748022907</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Gamgadhi Branch, Mugu</td>
                                            <td>9748002000</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Dullu Branch, Dailekh</td>
                                            <td>089-411051</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Bidhyapur Branch, Surkhet</td>
                                            <td>9848027151</td>
                                        </tr>
                                        <tr>
                                            <td>I</td>
                                            <td>Regional Office Pokhara, Kaski</td>
                                            <td>061-541380</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Babiyachaur, Myagdi</td>
                                            <td>068-690755</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Burtibang, Baglung</td>
                                            <td>068-410023</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Dulegauda, Tanahu</td>
                                            <td>065-570860</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Galkot, Baglung</td>
                                            <td>068-690754</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Galyang, Syangja</td>
                                            <td>063-460287</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Patichaur, Parbat</td>
                                            <td>067-690583</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Pokhara, Kaski</td>
                                            <td>061-533243</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Putlibazzar, Syangja</td>
                                            <td>063-420922</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Rahale, Parbat</td>
                                            <td>9815253193</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Waling, Syangja</td>
                                            <td>063-440514</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Chame Branch, Manang</td>
                                            <td>9846197452</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Ghansa Branch, Mustang</td>
                                            <td>9847317692</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Gaaikhur Branch, Gorkha</td>
                                            <td>9845221568</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Duipiple Branch, Lamjung</td>
                                            <td>9845079788</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Kharwang Branch, Baglung</td>
                                            <td>9846065955</td>
                                        </tr>
                                        <tr>
                                            <td>J</td>
                                            <td>Regional Office Kathmandu, Kathmandu</td>
                                            <td>01-4415257</td>
                                        </tr>
                                        <tr>
                                            <td>1</td>
                                            <td>Galchhi, Dhadhing</td>
                                            <td>010-403017</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>Dhikure, Nuwakot</td>
                                            <td>010-681339</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>Chhatrebajh, Kavreplanchowk</td>
                                            <td>011-685823</td>
                                        </tr>
                                        <tr>
                                            <td>4</td>
                                            <td>Chhaling, Bhaktapur</td>
                                            <td>01-5091020</td>
                                        </tr>
                                        <tr>
                                            <td>5</td>
                                            <td>Dakshinkali, Kathmandu</td>
                                            <td>01-4710505</td>
                                        </tr>
                                        <tr>
                                            <td>6</td>
                                            <td>Gagalphedi, Kathmandu</td>
                                            <td>9845253623</td>
                                        </tr>
                                        <tr>
                                            <td>7</td>
                                            <td>Jethal, Sindhupalchowk</td>
                                            <td>9842506965</td>
                                        </tr>
                                        <tr>
                                            <td>8</td>
                                            <td>Namdu, Dolakha</td>
                                            <td>9847079664</td>
                                        </tr>
                                        <tr>
                                            <td>9</td>
                                            <td>Nawalpur, Sindhupalchowk</td>
                                            <td>9851176430</td>
                                        </tr>
                                        <tr>
                                            <td>10</td>
                                            <td>Ramkot, Kathmandu</td>
                                            <td>9847027217</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Tikathali, Lalitpur</td>
                                            <td>9847029530</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Tripureshor, Dhading</td>
                                            <td>9847040850</td>
                                        </tr>
                                        <tr>
                                            <td>13</td>
                                            <td>Thecho, Lalitpur</td>
                                            <td>9818146291</td>
                                        </tr>
                                        <tr>
                                            <td>14</td>
                                            <td>Kalikasthan Branch, Rasuwa</td>
                                            <td>9847848298</td>
                                        </tr>
                                        <tr>
                                            <td>15</td>
                                            <td>Palung Branch, Makwanpur</td>
                                            <td>9851054626</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Daunnedevi Branch, Nawalparasi</td>
                                            <td>078-620316</td>
                                        </tr>
                                        <tr>
                                            <td>11</td>
                                            <td>Jabgadi,Palpa</td>
                                            <td>071-696976</td>
                                        </tr>
                                        <tr>
                                            <td>19</td>
                                            <td>Bijaybasti Branch, Parsa</td>
                                            <td>053-691604</td>
                                        </tr>
                                        <tr>
                                            <td>12</td>
                                            <td>Juhaang,Gulmi</td>
                                            <td>071-696978</td>
                                        </tr>
                                        <tr>
                                            <td>21</td>
                                            <td>Paanchkanya Branch, Sunsari</td>
                                            <td>025-552131</td>
                                        </tr>
                                        <tr>
                                            <td>22</td>
                                            <td>Karsiya Branch, Morang</td>
                                            <td>021-565248</td>
                                        </tr>
                                        <tr>
                                            <td>23</td>
                                            <td>Prakashpur Branch, Sunsari</td>
                                            <td>021-565248</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <?php include('includes/sidebar.php'); ?>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
