<?php include('includes/header.php'); ?>
<div class="vtable img-banner saving-banner">
    <div class="table-cell vmiddle">
        <h2 class="text-uppercase right">Saving</h2>
    </div>
</div>
<div class="about-section nubl-section" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="f-content-wrapper nubl-content">
                <h2 class="text-uppercase">Savings to Group members</h2>

                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <strong>NUBL</strong> requires its clients to save with most of their loan products and
                        encourages clients to
                        contribute to voluntary savings accounts as well. Both savings accounts give members access to
                        funds for health, education and consumption needs either though withdrawal of voluntary savings.
                        There are 4 types of saving products offered to its clients, they are: compulsory saving,
                        voluntary saving, centre fund savings and recurring savings.
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <img src="img/generalsaving.jpg" alt="Saving to General Group Members">
                    </div>
                </div>
            </div>
            <div class="f-content-wrapper nubl-content">
                <h2 class="text-uppercase">Savings to General Public</h2>

                <div class="row">
                    <div class="col-xs-12 col-sm-8 col-md-8">
                        <strong>NUBL</strong> taking permission from the central bank (Nepal Rastra Bank), has started
                        colleting deposits
                        from general public. Under this category, NUBL offers Easy Saving, Special Savings, Employee
                        Saving accounts, Recurring deposit and fixed deposit scheme with maximum of 2 years maturity.
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4">
                        <img src="img/general-public.jpg" alt="Saving to General Public">
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <?php include('includes/sidebar.php'); ?>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
