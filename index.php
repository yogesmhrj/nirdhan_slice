<?php include('includes/header.php'); ?>

<!-- Carousel
    ================================================== -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="item active">
            <img class="first-slide" src="img/slider1.jpg" alt="First slide">
        </div>
        <div class="item">
            <img class="second-slide" src="img/slider1.jpg" alt="Second slide">
        </div>
        <div class="item">
            <img class="third-slide" src="img/slider1.jpg" alt="Third slide">
        </div>
    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div><!-- /.carousel -->

<div class="section-feat">
    <div class="row">
        <div class="col-md-5">
            <div class="section">
                <h4>Current & Personal Savings</h4>

                <div class="aside">
                    <span>NUBL requires its clients to save with most of their loan products and encourages clients to contribute to voluntary savings accounts as well</span>
                </div>
                <img class="sect-img" src="img/sect-1.jpg">

                <div class="btn-section">
                    <button class="btn btn-group-lg">Learn more about us</button>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="section">
                <h4>Help in your investment</h4>

                <div class="aside">
                    <span>NUBL is providing micro insurance product that address clients' needs for risk management in addition to its savings and credit products. </span>
                </div>
                <img class="sect-img" src="img/sect-2.jpg">

                <div class="btn-section">
                    <button class="btn btn-group-lg">Learn more about us</button>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="address">
                <span>Nirdhan Uthan Bank Limited</span><br>
                <span>Tel : <p>977-1- 4413711,<br>
                        4413794, 4413840</p></span><br>
                <span>Email: <a href="">info@nirdhan.com</a> </span>
            </div>
        </div>
    </div>
</div>
<div class="section-about">
    <div class="row">
        <div class="col-md-8">
            <div class="section">
                <h4>About Nirdhan Uthan Bank Limited</h4>

                <div class="aside">
                    <span>Nirdhan Utthan Bank Limited, "the bank for upliftment of the poor" is a microfinance bank established in November 1998 under Company Act of Nepal 1997 (now Company Act 2006). Nepal Rastra Bank, the Central Bank of Nepal, granted a license in April 1999 to undertake banking activities under the Development Bank Act 1996. It started its formal operation from July 1999. Now, operated under Bank and Financial Institutions Act 2006, Nirdhan Utthan Bank Limited (NUBL) provides microfinance services such as Loans, Deposits, Micro-insurance and Remittance services to low income families of Nepal. NUBL follows group lending based on Grameen Bank, Bangladesh model as well as group lending based on NUBL developed Self-Relaint Group model through a network of 178 branch offices spread over all 75 districts of Nepal.</span>
                </div>
                <img class="sect-img" src="img/nirdhan-building.jpg">

                <div class="btn-section">
                    <button class="btn btn-group-lg">Learn more about us</button>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="section">
                <h4>NUBL at Glance, 14 June 2016</h4>

                <div class="table">
                    <table class="table">
                        <tr>
                            <td>
                                Loan Outstanding (NPR)
                            </td>
                            <td>10.00 Billion</td>
                        </tr>
                        <tr>
                            <td>Saving Deposit (NPR)</td>
                            <td>3.98 Billion</td>

                        </tr>
                        <tr>
                            <td> Number of Active Clients
                            </td>
                            <td>
                                274,526
                            </td>
                        </tr>
                        <tr>
                            <td> Number of Loan Clients
                            </td>
                            <td>188,189</td>
                        </tr>
                        <tr>
                            <td> Number of Centres
                            </td>
                            <td>13,653</td>
                        </tr>
                        <tr>
                            <td> Total Staffs ( Including Trainee)
                            </td>
                            <td>874</td>
                        </tr>
                        <tr>
                            <td> Number of Branch Offices
                            </td>
                            <td>178</td>
                        </tr>
                        <tr>
                            <td> Number of Regional Offices
                            </td>
                            <td>10</td>
                        </tr>
                        <tr>
                            <td> Working Districts
                            </td>
                            <td>75</td>
                        </tr>
                        <tr>
                            <td> Working VDC & Municipalities
                            </td>
                            <td>1,754</td>
                        </tr>
                        <tr>
                            <td> Number of Female Staffs
                            </td>
                            <td>170</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section-news">
    <h3>Your news and information</h3>
    <div class="row">
        <div class="col-md-3">
            <div class="news-content">
                <a href="#news">
                    <h4>What's new</h4>
                    <img src="img/news-1.jpg" alt="Whats News">
                    <p>We are launching our tablet banking services all our country districts</p>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="news-content">
                <a href="#news">
                    <h4>Help with your home loan
                        payments</h4>
                    <img src="img/news-2.jpg" alt="Whats News">
                    <p>We are launching our tablet banking services all our country districts</p>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="news-content">
                <a href="#news">
                    <h4>Safety Tips</h4>
                    <img src="img/news-3.jpg" alt="Whats News">
                    <p>We are launching our tablet banking services all our country districts</p>
                </a>
            </div>
        </div>
        <div class="col-md-3">
            <div class="news-content">
                <a href="#news">
                    <h4>Savings & Investment</h4>
                    <img src="img/news-4.jpg" alt="Whats News">
                    <p>We are launching our tablet banking services all our country districts</p>
                </a>
            </div>
        </div>

    </div>
</div>

<?php include('includes/footer.php'); ?>
