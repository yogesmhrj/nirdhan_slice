<?php include('includes/header.php'); ?>

<div class="nubl-section news-nubl">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8">
            <div class="f-content-wrapper nubl-content">
                <h2 class="text-uppercase">Nirdhan Team Members</h2>

                <div class="content-team">
                    <div class="team-mem">
                        <div class="team-photo"><img src="img/fUSG_vOs.jpg"> </div>
                        <h3 class="text-capitalize">John Doe</h3>
                        <span class="text-capitalize">General Manager</span>
                        <p class="team-descp">passionate person.</p>
                        <ul>
                            <li><a href="#face"><img src="img/icon/fb-icon.png"> </a> </li>
                            <li><a href="#face"><img src="img/icon/twit-icon.png"> </a> </li>
                            <li><a href="#face"><img src="img/icon/linkedin-icon.png"> </a> </li>
                        </ul>
                    </div>
                    <div class="team-mem">
                        <div class="team-photo"><img src="img/fUSG_vOs.jpg"> </div>
                        <h3 class="text-capitalize">John Doe</h3>
                        <span class="text-capitalize">General Manager</span>
                        <p class="team-descp">passionate person.</p>
                        <ul>
                            <li><a href="#face"><img src="img/icon/fb-icon.png"> </a> </li>
                            <li><a href="#face"><img src="img/icon/twit-icon.png"> </a> </li>
                            <li><a href="#face"><img src="img/icon/linkedin-icon.png"> </a> </li>
                        </ul>
                    </div>
                    <ul class="pagination">
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <?php include('includes/sidebar.php'); ?>
        </div>
    </div>
</div>


<?php include('includes/footer.php'); ?>
