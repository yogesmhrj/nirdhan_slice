<div class="f-common-wrapper">
    <h4>NUBL at Glance, 14 June 2016</h4>
    <div class="table">
        <table class="table table-striped">
            <tr>
                <td>
                    Loan Outstanding (NPR)
                </td>
                <td>10.00 Billion</td>
            </tr>
            <tr>
                <td>Saving Deposit (NPR)</td>
                <td>3.98 Billion</td>

            </tr>
            <tr>
                <td> Number of Active Clients
                </td>
                <td>
                    274,526
                </td>
            </tr>
            <tr>
                <td> Number of Loan Clients
                </td>
                <td>188,189</td>
            </tr>
            <tr>
                <td> Number of Centres
                </td>
                <td>13,653</td>
            </tr>
            <tr>
                <td> Total Staffs ( Including Trainee)
                </td>
                <td>874</td>
            </tr>
            <tr>
                <td> Number of Branch Offices
                </td>
                <td>178</td>
            </tr>
            <tr>
                <td> Number of Regional Offices
                </td>
                <td>10</td>
            </tr>
            <tr>
                <td> Working Districts
                </td>
                <td>75</td>
            </tr>
            <tr>
                <td> Working VDC & Municipalities
                </td>
                <td>1,754</td>
            </tr>
            <tr>
                <td> Number of Female Staffs
                </td>
                <td>170</td>
            </tr>
        </table>
    </div>
</div>
<div class="f-common-wrapper">
    <a href="saving.php">
        <img src="img/general-public.jpg" alt="saving">
    </a>
</div>